//
//  OpenCLTest.swift
//  OSUOpenCL
//
//  Created by William Dillon on 12/3/14.
//  Copyright (c) 2014 CEOAS. All rights reserved.
//

import XCTest
@testable import OSUOpenCL

class OSUOpenCLTest: XCTestCase {
    var ocl: OSUOpenCL = OSUOpenCL.sharedSystem()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testDescription() {
        print(ocl.description())
    }
    
    func testContextCPU() {
        do {
            let _ = try OSUOpenCLContext(type: OSUOpenCL.DeviceType.cpu)
        } catch let error {
            assert(false, "OpenCL Context failed to initalize: \(error)")
        }
    }
    
    func testContextGPU() {
        do {
            let _ = try OSUOpenCLContext(type: OSUOpenCL.DeviceType.gpu)
        } catch let error {
            assert(false, "OpenCL Context failed to initalize: \(error)")
        }
    }
    
    // Because this system has none of this type, expect failure
    func testContextAccelerator() {
        do {
            let _ = try OSUOpenCLContext(type: OSUOpenCL.DeviceType.accelerator)
        } catch let error {
            assert(false, "OpenCL Context failed to initalize: \(error)")
        }
    }
    
    // Because this system (should) has none of this type, expect failure
    func testContextCustom() {
        do {
            let _ = try OSUOpenCLContext(type: OSUOpenCL.DeviceType.custom)
        } catch let error {
            assert(false, "OpenCL Context failed to initalize: \(error)")
        }
    }
    
    func testContextAll() {
        do {
            let _ = try OSUOpenCLContext(type: OSUOpenCL.DeviceType.all)
        } catch let error {
            assert(false, "OpenCL Context failed to initalize: \(error)")
        }
    }
    
    func testContextDefault() {
        do {
            let _ = try OSUOpenCLContext(type: OSUOpenCL.DeviceType.sysDefault)
        } catch let error {
            assert(false, "OpenCL Context failed to initalize: \(error)")
        }
    }
    
    func testContextList() {
        do {
            let _ = try OSUOpenCLContext(type: OSUOpenCL.DeviceType.all)
        } catch let error {
            assert(false, "OpenCL Context failed to initalize: \(error)")
        }
    }
    
    func testProgramCompile() {
        do {
            let context = try OSUOpenCLContext(type: OSUOpenCL.DeviceType.all)
            
            // Get the path
            let sourcePath: String! = "/Users/wdillon/Documents/Source Code/OpenCL/OSUOpenCL/OSUOpenCLTests/r2c1024FFT.cl"
            assert(sourcePath != nil, "Unable to create path to OpenCL Source code.")

            let source: String
            do {
                source = try String(contentsOfFile: sourcePath, encoding: NSUTF8StringEncoding)
            } catch let error {
                assert(false, "Unable to read OpenCL Source code for test: \(error)")
                return
            }

            // Create a program object
            let program = OSUOpenCLProgram(context: context, source: source)
            XCTAssert(program != nil, "Creation of program object failed: ")
            
            // Let's create an options array that has every options to check for issues
            var options = Array<OSUOpenCLProgram.BuildOption>()
            options.append(.Define("TEST_DEFINE"))
            options.append(.Include("testInclude.inc"))
            options.append(.DenormsAreZero)
    //        options.append(.DisableOptimization)
    //        options.append(.EnableMAD)
            options.append(.FiniteMathOnly)
            options.append(.UnsafeMathOptimization)
            options.append(.StrictAliasing)
    //        options.append(.NoSignedZero)
            options.append(.FastRelaxedMath)
            options.append(.WarningsAreErrors)
            options.append(.SupressWarnings)
            
            // Compile the program
            let result = program!.compile(context.devices, options: options)
            assert(result == true, "OpenCL compile should have succeeded, but didn't \n\(program!.buildLog())")
            
            // Create the kernels from the program
            let kernels = program!.kernels()
            assert(kernels != nil, "Unable to load kernels from OpenCL Program.")
            assert(kernels!.count != 0, "No kernels found in program, there should have been 1")
        } catch let error {
            assert(false, "Unable to create an OSUOpenCL context: \(error)")
        }
    }
    
    func testProgramCompileFail() {
        do {
            let context = try OSUOpenCLContext(type: OSUOpenCL.DeviceType.all)
            let sourcePath = "/Users/wdillon/Documents/Source Code/OpenCL/OSUOpenCL/OSUOpenCLTests/r2c1024FFT-broken.cl"
            let source = try String(contentsOfFile: sourcePath, encoding: NSUTF8StringEncoding)
            let program = OSUOpenCLProgram(context: context, source: source)

            assert(program != nil, "Creation of program object failed: ")
            
            let result = program!.compile(context.devices)
            assert(result == false, "OpenCL compile shouldn't have succeeded, but did.")
            
        } catch let error {
            assert(false, "Unable to create an OSUOpenCL context: \(error)")
        }
    }
    
    func testKernel() {
        // Each of these steps is independently tested in other tests
        do {
            let context = try OSUOpenCLContext(type: OSUOpenCL.DeviceType.all)
            let sourcePath = "/Users/wdillon/Documents/Source Code/OpenCL/OSUOpenCL/OSUOpenCLTests/r2c1024FFT.cl"
            let source = try String(contentsOfFile: sourcePath, encoding: NSUTF8StringEncoding)
            var program = OSUOpenCLProgram(context: context, source: source)
            let result = program!.compile(context.devices)
            let kernels = program!.kernels()
        } catch let error {
            assert(false, "Unable to load all context: \(error)")
        }
    }
}

extension OSUOpenCLTest {
    static var allTests: [(String, OSUOpenCLTest -> () throws -> Void)] {
        return [
            ("testDescription",testDescription),
            ("testContextCPU",testContextCPU),
            ("testContextGPU",testContextGPU),
            ("testContextAccelerator",testContextAccelerator),
            ("testContextCustom",testContextCustom),
            ("testContextAll",testContextAll),
            ("testContextDefault",testContextDefault),
            ("testContextList",testContextList),
            ("testProgramCompile",testProgramCompile),
            ("testProgramCompileFail",testProgramCompileFail),
            ("testKernel",testKernel)
        ]
    }
}
