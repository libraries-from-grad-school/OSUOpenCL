//
//  main.swift
//  OSUOpenCL
//
//  Created by William Dillon on 2/23/16.
//  Copyright © 2016 Oregon State University. All rights reserved.
//

import XCTest

@testable import OSUOpenCLtest

XCTMain([
    OSUOpenCL_test()
])
