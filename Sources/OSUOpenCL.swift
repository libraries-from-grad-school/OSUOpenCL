//
//  OSUOpenCL.swift
//  OSUOpenCL
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import Foundation
import OpenCL
//import OSULogger

internal var globalOSUOpenCL = try! OSUOpenCL()

enum OSUOpenCLError: ErrorType {
    case deviceStringQueryError(optional: String)
    case deviceScalarQueryError(optional: String)
    case deviceVectorQueryError(optional: String)
    
    case platformStringQueryError(optional: String)
    
    case openCLInitializeError(optional: String)
    case kernelInitializeError(optional: String)
    case contextInitializeError(optional: String)
}

public class OSUOpenCL {
    public private(set) var name:      String = ""
    public private(set) var profile:   String = ""
    public private(set) var version:   String = ""
    public private(set) var vendor:    String = ""
    
    // This enum is only for use with the Swift part of the API.  Swift
    // prevents the use of the raw type used in OpenCL because those values
    // are exported as functions rather than defines.
    public enum DeviceType {
        case cpu
        case gpu
        case accelerator
        case custom
        case all
        case unknown
        case sysDefault
        
        func toRaw() -> cl_device_type {
            var output: UInt64 = 0
            switch self {
            case cpu: output = UInt64(CL_DEVICE_TYPE_CPU)
            case gpu: output = UInt64(CL_DEVICE_TYPE_GPU)
            case accelerator: output = UInt64(CL_DEVICE_TYPE_ACCELERATOR)
            case custom: output = UInt64(CL_DEVICE_TYPE_CUSTOM)
            case all: output = UInt64(CL_DEVICE_TYPE_ALL)
            case sysDefault: output = UInt64(CL_DEVICE_TYPE_DEFAULT)
            default: output = 0
            }
            
            return cl_device_type(output)
        }
        
        public func toString() -> String {
            switch self {
            case cpu: return "CPU"
            case gpu: return "GPU"
            case accelerator: return "Accelerator"
            case custom: return "Custom"
            case all: return "All"
            case sysDefault: return "Default"
            default: return "Unknown"
            }
        }
    }
    
    public private(set) var devices = [OSUOpenCLDevice]()
    public private(set) var devicesByID = [cl_device_id: OSUOpenCLDevice]()
    
    public class func sharedSystem() -> OSUOpenCL {
        return globalOSUOpenCL
    }
    
//    var kernels: [OSUOpenCLKernel] = []
    
    // It is entirely possible that the OpenCL system doesn't work.  It's not
    // likely, but if it happens, there's nothing that we can do about it.
    private init() throws {
        do {
            // Retreive the platform information strings from the API
            name      = try OSUOpenCL.platformInfoString(CL_PLATFORM_NAME)
            profile   = try OSUOpenCL.platformInfoString(CL_PLATFORM_PROFILE)
            version   = try OSUOpenCL.platformInfoString(CL_PLATFORM_VERSION)
            vendor    = try OSUOpenCL.platformInfoString(CL_PLATFORM_VENDOR)
            
            // Variables needed to get a list of devices
            var numDevices: cl_uint = 0
            var retval: cl_int = CL_SUCCESS
            
            // Get the number of devices (so we know how big of a buffer to make
            retval = clGetDeviceIDs(nil, cl_device_type(CL_DEVICE_TYPE_ALL), 0, nil, &numDevices)
            if retval != CL_SUCCESS {
                throw OSUOpenCLError.openCLInitializeError(optional: "Unable to get devices from OpenCL: \(OSUOpenCL.printCLError(retval))")
            }
            
            // Create the buffer
            var tempDevices = [cl_device_id](count: Int(numDevices), repeatedValue: cl_device_id(bitPattern: 0))
            
            // Get the devices
            retval = clGetDeviceIDs(nil, cl_device_type(CL_DEVICE_TYPE_ALL), numDevices, &tempDevices, nil)
            if retval != CL_SUCCESS {
                throw OSUOpenCLError.openCLInitializeError(optional: "Unable to get devices from OpenCL: \(OSUOpenCL.printCLError(retval))")
            }
            
            // For each device ID, create a OSUOpenCLDevice and add it to our array and dict
            for tempDeviceID in tempDevices {
                do {
                    let optDevice: OSUOpenCLDevice = try OSUOpenCLDevice(deviceID: tempDeviceID)
                    devices.append(optDevice)
                    devicesByID[tempDeviceID] = optDevice
                } catch let error {
                    print("Creation of a swift device from OpenCL device id \(tempDeviceID) failed: \(error).")
                }
            }
        } catch let error {
            throw error
        }
    }

    public func description() -> String {
        var tempString = ""
        
        tempString += "\(self.version) \(self.profile) by \(self.vendor).\n"
        for device in devices {
            tempString += device.description()
        }
        
        return tempString
    }
    
    // The input parameter is Int32 because that's what the CL_PLATFORM...
    // constants are defined as.  It's not clear why that's the case when
    // the clGet... functions expect cl_platform_info, which are cl_uint
    public class func platformInfoString(paramIn: Int32) throws -> String {
        let param: cl_platform_info = cl_platform_info(paramIn)
        var returnBufferSize: Int = 0
        var retval: cl_int = CL_SUCCESS
        
        // Try to get the length of the required buffer
        retval = clGetPlatformInfo(nil, param, 0, nil, &returnBufferSize)
        
        // Did that fail?
        if retval != CL_SUCCESS {
            throw OSUOpenCLError.platformStringQueryError(optional: "Unable to get required query buffer length: \(OSUOpenCL.printCLError(retval))")
        }
        
        // Allocate a buffer to contain the value (which is UTF8 string encoded)
        var buffer: [CChar] = Array<CChar>(count: Int(returnBufferSize),
            repeatedValue: CChar(0x00))
        
        // Attempt to retreive the buffer
        retval = clGetPlatformInfo(nil, param, returnBufferSize, &buffer, nil)
        
        // Check for failure
        if retval != CL_SUCCESS {
            throw OSUOpenCLError.platformStringQueryError(optional: "Unable to get platform buffer: \(OSUOpenCL.printCLError(retval))")
        }
        
        // Convert the array of CChars into a string and return
        if let str = String(UTF8String: buffer) {
            return str
        } else {
            throw OSUOpenCLError.platformStringQueryError(optional: "Unable to convert byte buffer to string.")
        }
    }


    public class func printCLError(errorVal: cl_int) -> String {
        switch (errorVal) {
            case CL_SUCCESS:                         return "Success"
            case CL_DEVICE_NOT_FOUND:                return "Device not found"
            case CL_DEVICE_NOT_AVAILABLE:            return "Device Not Available"
            case CL_COMPILER_NOT_AVAILABLE:          return "Compiler not available"
            case CL_MEM_OBJECT_ALLOCATION_FAILURE:   return "Unable to allocate Memory"
            case CL_OUT_OF_RESOURCES:                return "Mem object allocation failure"
            case CL_OUT_OF_HOST_MEMORY:              return "Out of Host Memory"
            case CL_PROFILING_INFO_NOT_AVAILABLE:    return "Profiling not available"
            case CL_MEM_COPY_OVERLAP:                return "Memory copy overlap"
            case CL_IMAGE_FORMAT_MISMATCH:           return "Image format mismatch"
            case CL_IMAGE_FORMAT_NOT_SUPPORTED:      return "Image format not supported"
            case CL_BUILD_PROGRAM_FAILURE:           return "Program build failed"
            case CL_MAP_FAILURE:                     return "Memory map failure"
            case CL_INVALID_VALUE:                   return "Invalid value"
            case CL_INVALID_DEVICE_TYPE:             return "Invalid device type"
            case CL_INVALID_PLATFORM:                return "Invalid platform"
            case CL_INVALID_DEVICE:                  return "Invalid Device"
            case CL_INVALID_CONTEXT:                 return "Invalid Context"
            case CL_INVALID_QUEUE_PROPERTIES:        return "Invalid Queue Properties"
            case CL_INVALID_COMMAND_QUEUE:           return "Invalid Command Queue"
            case CL_INVALID_HOST_PTR:                return "Invalid Host Pointer"
            case CL_INVALID_MEM_OBJECT:              return "Invalid Memory Object"
            case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR: return "Invalid image format descriptor"
            case CL_INVALID_IMAGE_SIZE:              return "Invalid image size"
            case CL_INVALID_SAMPLER:                 return "Invalid sampler"
            case CL_INVALID_BINARY:                  return "Invalid Binary"
            case CL_INVALID_BUILD_OPTIONS:           return "Invalid Build Options"
            case CL_INVALID_PROGRAM:                 return "Invalid Program"
            case CL_INVALID_PROGRAM_EXECUTABLE:      return "Invalid Program Executable"
            case CL_INVALID_KERNEL_NAME:             return "Invalid Kernel Name"
            case CL_INVALID_KERNEL_DEFINITION:       return "Invalid Kernel Definition"
            case CL_INVALID_KERNEL:                  return "Invalid Kernel"
            case CL_INVALID_ARG_INDEX:               return "Invalid argument index"
            case CL_INVALID_ARG_VALUE:               return "Invalid argument value"
            case CL_INVALID_ARG_SIZE:                return "Invalid argument size"
            case CL_INVALID_KERNEL_ARGS:             return "Invalid kernel arguments"
            case CL_INVALID_WORK_DIMENSION:          return "Invalid work dimensions"
            case CL_INVALID_WORK_GROUP_SIZE:         return "Invalid work group size"
            case CL_INVALID_WORK_ITEM_SIZE:          return "Invalid work item size"
            case CL_INVALID_GLOBAL_OFFSET:           return "Invalid global offset"
            case CL_INVALID_EVENT_WAIT_LIST:         return "Invalid event wait list"
            case CL_INVALID_EVENT:                   return "Invalid event"
            case CL_INVALID_OPERATION:               return "Invalid Operation"
            case CL_INVALID_GL_OBJECT:               return "Invalid OpenGL object"
            case CL_INVALID_BUFFER_SIZE:             return "Invalid Buffer Size"
            case CL_INVALID_MIP_LEVEL:               return "Invalid MIP level"
            default:                                 return "Unknown error code"
        }
    }
}
