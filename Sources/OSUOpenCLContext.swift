//
//  OSUOpenCLContext.swift
//  OSUOpenCL
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import Foundation
import OpenCL
//import OSULogger

public class OSUOpenCLContext {
    public var devices = [OSUOpenCLDevice]()
    var clID: cl_context = nil
    
    public init(devicesIn: [OSUOpenCLDevice]) throws {
        if devicesIn.count == 0 {
            throw OSUOpenCLError.contextInitializeError(optional: "Unable to create a context with no devices.")
        }
        
        // Get an array of OpenCL device ids (in the API representation) from
        // the swift objects
        var cl_devices = [cl_device_id](count: devices.count, repeatedValue: cl_device_id(bitPattern: 0))
        cl_devices = devicesIn.map({ (device: OSUOpenCLDevice) -> cl_device_id in
            return device.clID
        })
        
        // Create the context using the new array of device IDs.
        var error: cl_int = 0
        clID = clCreateContext(nil, cl_uint(cl_devices.count), &cl_devices, nil, nil, &error)
        if error != CL_SUCCESS || clID == nil {
            var errorString = "Error creating context (\(OSUOpenCL.printCLError(error))) with devices: \n"
            for device in devicesIn {
                errorString += "\t\(device.name)\n"
            }
            throw OSUOpenCLError.contextInitializeError(optional: errorString)
        }
        
        // For now, let's just copy the input array.  It may be more robust to 
        // get the list again from the API to make sure that it's what we think
        // it is.
        do {
            devices = try queryDevices()
        } catch let error {
            throw error
        }
    }
    
    // These initalizers exist to create contexts of a given device type
    public init(type: OSUOpenCL.DeviceType) throws {
        var error: cl_int = 0
        clID = clCreateContextFromType(nil, type.toRaw(), nil, nil, &error)
        if error != CL_SUCCESS || clID == nil {
            throw OSUOpenCLError.contextInitializeError(optional: "Error creating context (\(OSUOpenCL.printCLError(error))) with \(type.toString()) type.\n")
        }
        
        // For now, let's just copy the input array.  It may be more robust to
        // get the list again from the API to make sure that it's what we think
        // it is.
        do {
            devices = try queryDevices()
        } catch let error {
            throw error
        }
    }
    
    private func queryDevices() throws -> [OSUOpenCLDevice] {
        var returnSize: Int = 0
        var retval: cl_int = 0
        
        retval = clGetContextInfo(clID, cl_context_info(CL_CONTEXT_DEVICES), 0, nil, &returnSize)
        if retval != CL_SUCCESS {
            throw OSUOpenCLError.contextInitializeError(optional: "Unable to query context for device list.")
        }
        
        var cl_devices: Array<cl_device_id>
        cl_devices = Array<cl_device_id>(count: Int(returnSize/Int(sizeof(cl_device_id))),
            repeatedValue: cl_device_id(bitPattern: 0))
        retval = clGetContextInfo(clID, cl_context_info(CL_CONTEXT_DEVICES), returnSize, &cl_devices, nil)
        if retval != CL_SUCCESS {
            throw OSUOpenCLError.contextInitializeError(optional: "Unable to query context for device list.")
        }
        
        // Translate the cl_device_id entries into OSUOpenCLDevice objects
        var tempDevices = [OSUOpenCLDevice]()
        for cl_device in cl_devices {
            if let swiftDevice = globalOSUOpenCL.devicesByID[cl_device] {
                tempDevices.append(swiftDevice)
            } else {
                throw OSUOpenCLError.contextInitializeError(optional: "OpenCL Context returned a device that doesn't seem to exist.")
            }
        }
        
        return tempDevices
    }
    
    public func description() -> String {
        var tempString = ""
        
        tempString += "OpenCL Context pointer: \(clID)\n"
        tempString += "Devices: \n"
        for device in devices {
            tempString += "\t\(device.name)\n"
        }
        
        return tempString
    }
}