//
//  OSUOpenCLDevice.swift
//  OSUOpenCL
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import Foundation
import OpenCL

public class OSUOpenCLDevice {
    public private(set)var clID:               cl_device_id    = nil
    
    public private(set)var name:                   String      = ""
    public private(set)var vendor:                 String      = ""
    public private(set)var driverVersion:          String      = ""
    public private(set)var deviceVersion:          String      = ""
    public private(set)var profile:                String      = ""
    public private(set)var extensions:             String      = ""

    public private(set)var type:     OSUOpenCL.DeviceType      = OSUOpenCL.DeviceType.unknown
    public private(set)var vendorID:               UInt32      = 0
    
    public private(set)var littleEndian:           Bool        = false
    public private(set)var available:              Bool        = false
    public private(set)var compilerAvailable:      Bool        = false
    
    public private(set)var maxComputeUnits:        UInt32      = 0
    public private(set)var maxClockFrequency:      UInt32      = 0
    public private(set)var profileTimerRes:        UInt64      = 0
    
    public private(set)var maxWorkItemDim:         UInt32      = 0
    public private(set)var maxWorkItemSize:      [UInt64]      = [0]
    public private(set)var maxWorkGroupSize:       UInt64      = 0
    
    public private(set)var imageSupport:           Bool        = false
    public private(set)var maxWriteImageArgs:      UInt32      = 0
    public private(set)var maxReadImageArgs:       UInt32      = 0
    public private(set)var maxImage2dSize:         (UInt64, UInt64) = (0, 0)
    public private(set)var maxImage3dSize:         (UInt64, UInt64, UInt64) = (0, 0, 0)
    public private(set)var maxImageSamplers:       UInt32      = 0
    
    public private(set)var maxParameterSize:       UInt64      = 0
    
    public private(set)var addressBits:            UInt32      = 0
    public private(set)var addressAlignment:       UInt32      = 0
    public private(set)var minDataTypeAlignSize:   UInt32      = 0
    public private(set)var maxMemAllocSize:        UInt64      = 0
    public private(set)var memType: cl_device_local_mem_type   = 0
    public private(set)var memSize:                UInt64      = 0
    public private(set)var memErrorCorrection:     Bool        = false
    
    public private(set)var memCacheType:           cl_device_local_mem_type = 0
    public private(set)var memCacheLineSize:       UInt32      = 0
    public private(set)var memGlobalCacheSize:     UInt64      = 0
    public private(set)var memGlobalSize:          UInt64      = 0
    public private(set)var maxConstBufferSize:     UInt64      = 0
    public private(set)var maxConstArgs:           UInt32      = 0
    
    public private(set)var preferedVecWidthChar:   UInt32      = 0
    public private(set)var preferedVecWidthShort:  UInt32      = 0
    public private(set)var preferedVecWidthInt:    UInt32      = 0
    public private(set)var preferedVecWidthLong:   UInt32      = 0
    public private(set)var preferedVecWidthFloat:  UInt32      = 0
    public private(set)var preferedVecWidthDouble: UInt32      = 0

    public private(set)var singleFpConfig:         cl_device_fp_config = 0
    
    // The input parameter is Int32 because that's what the CL_DEVICE_...
    // constants are defined as.  It's not clear why that's the case when
    // the clGet... functions expect cl_platform_info, which are cl_uint
    private class func deviceInfoString(deviceID: cl_device_id, param paramIn: Int32) throws -> String {
        let param = cl_device_info(paramIn)
        var returnBufferSize: Int = 0
        var retval = CL_SUCCESS
        
        // Try to get the length of the required buffer
        retval = clGetDeviceInfo(deviceID, param, 0, nil, &returnBufferSize)
        guard retval == CL_SUCCESS else {
            throw OSUOpenCLError.deviceStringQueryError(optional: OSUOpenCL.printCLError(retval))
        }
        
        // Allocate a buffer to contain the value (which is UTF8 string encoded)
        var buffer: [CChar] = Array<CChar>(count: Int(returnBufferSize), repeatedValue: CChar(0x00))
        
        retval = clGetDeviceInfo(deviceID, param, returnBufferSize, &buffer, nil)
        guard retval == CL_SUCCESS else {
            throw OSUOpenCLError.deviceStringQueryError(optional: OSUOpenCL.printCLError(retval))
        }
        
        // Convert the array of CChars into a string and return
        if let str = String(UTF8String: buffer) {
            return str
        } else {
            throw OSUOpenCLError.deviceStringQueryError(optional: "Unable to convert byte buffer into string")
        }
    }
    
    private class func deviceInfoVector(deviceID: cl_device_id, param paramIn: Int32) throws -> [UInt64] {
        let param: cl_device_info = cl_device_info(paramIn)
        var returnBufferSize: Int = 0
        var retval = CL_SUCCESS
        
        // Try to get the length of the required buffer
        retval = clGetDeviceInfo(deviceID, param, 0, nil, &returnBufferSize)
        guard retval == CL_SUCCESS else {
            throw OSUOpenCLError.deviceVectorQueryError(optional: OSUOpenCL.printCLError(retval))
        }
        
        // Allocate a buffer to contain the value
        var buffer: [UInt64] = Array<UInt64>(count: Int(returnBufferSize), repeatedValue: 0)
        
        retval = clGetDeviceInfo(deviceID, param, returnBufferSize, &buffer, nil)
        guard retval == CL_SUCCESS else {
            throw OSUOpenCLError.deviceVectorQueryError(optional: OSUOpenCL.printCLError(retval))
        }
        
        return buffer
    }

    private class func deviceInfoScalar<T: IntegerLiteralConvertible>(deviceID: cl_device_id, param paramIn: Int32) throws -> T {
        var returnBufferSize: size_t = 0
        let param = cl_platform_info(paramIn)
        
        let retval = clGetDeviceInfo(deviceID, param, 0, nil, &returnBufferSize)
        guard retval == CL_SUCCESS else {
            throw OSUOpenCLError.deviceScalarQueryError(optional: OSUOpenCL.printCLError(retval))
        }
        
        assert(returnBufferSize == size_t(sizeof(T)), "Type size mismatch while querying OpenCL. \(returnBufferSize) != \(sizeof(T)).")
        
        var output: T = 0

        guard clGetDeviceInfo(deviceID, param, returnBufferSize, &output, nil) == CL_SUCCESS else {
            throw OSUOpenCLError.deviceScalarQueryError(optional: OSUOpenCL.printCLError(retval))
        }

        return output
    }
    
    private class func deviceInfoScalar(deviceID: cl_device_id, param paramIn: Int32) throws -> Bool {
        do {
            let testValue: Int32 = try OSUOpenCLDevice.deviceInfoScalar(deviceID, param: paramIn)
            return testValue == 1
        } catch let error1 {
            throw error1
        }
    }
    
    public init(deviceID: cl_device_id) throws {
        clID = deviceID

        do {
            // Collect device information strings
            name                = try OSUOpenCLDevice.deviceInfoString(clID, param: CL_DEVICE_NAME)
            vendor              = try OSUOpenCLDevice.deviceInfoString(clID, param: CL_DEVICE_VENDOR)
            driverVersion       = try OSUOpenCLDevice.deviceInfoString(clID, param: CL_DRIVER_VERSION)
            deviceVersion       = try OSUOpenCLDevice.deviceInfoString(clID, param: CL_DEVICE_VERSION)
            profile             = try OSUOpenCLDevice.deviceInfoString(clID, param: CL_DEVICE_PROFILE)
            extensions          = try OSUOpenCLDevice.deviceInfoString(clID, param: CL_DEVICE_EXTENSIONS)
            vendorID            = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_VENDOR_ID)

            let rawDeviceType: Int64 = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_TYPE)
            switch (Int32(rawDeviceType)) {
                case CL_DEVICE_TYPE_CPU:         type = OSUOpenCL.DeviceType.cpu
                case CL_DEVICE_TYPE_GPU:         type = OSUOpenCL.DeviceType.gpu
                case CL_DEVICE_TYPE_ACCELERATOR: type = OSUOpenCL.DeviceType.accelerator
                case CL_DEVICE_TYPE_CUSTOM:      type = OSUOpenCL.DeviceType.custom
                default:                         type = OSUOpenCL.DeviceType.unknown
            }
            
            littleEndian        = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_ENDIAN_LITTLE)
            available           = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_AVAILABLE)
            compilerAvailable   = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_COMPILER_AVAILABLE)
            
            maxComputeUnits     = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_MAX_COMPUTE_UNITS)
            maxClockFrequency   = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_MAX_CLOCK_FREQUENCY)
            profileTimerRes     = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_PROFILING_TIMER_RESOLUTION)
            
            maxWorkItemDim      = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS)
            maxWorkGroupSize    = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_MAX_WORK_GROUP_SIZE)

            // Truncate the list of dimensions to only those that OpenCL promised
            // to provide.  The raw value I got from the API included 24 numbers,
            // but all but the first 3 were '0'.  It's clearly not useful to have
            // dimensions with a maximum size of 0.  This would make the non-zero
            // computation of the multi-dimensional volume impossible.
            var tempMaxWorkItemSize = [UInt64]()
            let tempSizes: [UInt64] = try OSUOpenCLDevice.deviceInfoVector(clID, param: CL_DEVICE_MAX_WORK_ITEM_SIZES)
            for i in 0 ..< maxWorkItemDim { tempMaxWorkItemSize.append(tempSizes[Int(i)]) }
            maxWorkItemSize = tempMaxWorkItemSize

            imageSupport         = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_IMAGE_SUPPORT)
            maxWriteImageArgs    = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_MAX_WRITE_IMAGE_ARGS)
            maxReadImageArgs     = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_MAX_READ_IMAGE_ARGS)

            var tempX: UInt64    = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_IMAGE2D_MAX_WIDTH)
            var tempY: UInt64    = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_IMAGE2D_MAX_HEIGHT)
            maxImage2dSize       = (tempX, tempY)
            tempX                = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_IMAGE3D_MAX_WIDTH)
            tempY                = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_IMAGE3D_MAX_HEIGHT)
            let tempZ: UInt64    = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_IMAGE3D_MAX_DEPTH)
            maxImage3dSize       = (tempX, tempY, tempZ)
            
            maxImageSamplers     = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_MAX_SAMPLERS)
            
            maxParameterSize     = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_MAX_PARAMETER_SIZE)
            
            addressBits          = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_ADDRESS_BITS)
            addressAlignment     = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_MEM_BASE_ADDR_ALIGN)
            minDataTypeAlignSize = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE)
            maxMemAllocSize      = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_MAX_MEM_ALLOC_SIZE)
            memType              = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_LOCAL_MEM_TYPE)
            memSize              = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_LOCAL_MEM_SIZE)
            memErrorCorrection   = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_ERROR_CORRECTION_SUPPORT)
            
            memCacheType         = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_GLOBAL_MEM_CACHE_TYPE)
            memCacheLineSize     = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE)
            memGlobalCacheSize   = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_GLOBAL_MEM_CACHE_SIZE)
            memGlobalSize        = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_GLOBAL_MEM_SIZE)
            maxConstBufferSize   = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE)
            maxConstArgs         = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT)
            
            preferedVecWidthChar    = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR)
            preferedVecWidthShort   = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT)
            preferedVecWidthInt     = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT)
            preferedVecWidthLong    = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG)
            preferedVecWidthFloat   = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT)
            preferedVecWidthDouble  = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE)
            
            singleFpConfig      = try OSUOpenCLDevice.deviceInfoScalar(clID, param: CL_DEVICE_SINGLE_FP_CONFIG)
        } catch let error {
            throw error
        }
    }

    public func description() -> String {
        var tempString = ""
        
        tempString += "Device ID: \(clID), "
        tempString += "Type: \(OSUOpenCL.DeviceType.toString(type))"
        tempString += "\tName: \(name)\n"
        tempString += "\tVendor: \(vendor); id: \(vendorID)\n"
        tempString += "\tDevice Version: \(deviceVersion)\n"
        tempString += "\tDriver Version: \(driverVersion)\n"
        tempString += "\tProfile: \(profile)\n"
        
        tempString += (available == true ? "\tDevice available\n" : "\tDevice unavailable\n")
        tempString += "\tCompiler "
        tempString += (compilerAvailable == true ? "available\n" : "unavailable\n")
        
        tempString += (littleEndian == true ? "\tLittle Endian\n" : "\tBig Endian\n")
        tempString += "\tError correction "
        tempString += (memErrorCorrection == true ? "available\n" : "unavailable\n")
        tempString += "\tDevice Extensions: \(extensions)\n"
        tempString += "\tMaximum compute units: \(maxComputeUnits)\n"
        tempString += "\tMaximum clock frequency: \(maxClockFrequency)Mhz\n"
        tempString += "\tPreferred vector widths:\n"
        tempString += "\t\tchar... \(preferedVecWidthChar)\n"
        tempString += "\t\tshort.. \(preferedVecWidthShort)\n"
        tempString += "\t\tint.... \(preferedVecWidthInt)\n"
        tempString += "\t\tlong... \(preferedVecWidthLong)\n"
        tempString += "\t\tfloat.. \(preferedVecWidthFloat)\n"
        tempString += "\t\tdouble. \(preferedVecWidthDouble)\n"
        
        tempString += "\tMaximum Workgroup size: \(maxWorkGroupSize)\n"
        tempString += "\tMaximum work item dimensions: \(maxWorkItemDim)\n"
        tempString += "\tMaximum work item sizes: \(maxWorkItemSize)\n"
        
        tempString += "\tGlobal memory size: \(memGlobalSize)\n"
        tempString += "\tMemory address alignment: \(addressAlignment)\n"
        tempString += "\tMemory default address bits: \(addressBits)\n"
        tempString += "\tMinimum data type alignment size: \(minDataTypeAlignSize)\n"
        tempString += "\tMaximum alloc size: \(maxMemAllocSize)\n"
        
        if (memCacheType == UInt32(CL_NONE)) {
            tempString += "\tNo global memory cache\n"
        } else {
            tempString += "\tGlobal memory "
            tempString += (memCacheType == UInt32(CL_READ_ONLY_CACHE) ?
                "read-only cache\n" : "read-write cache\n")
            tempString += "\t\tGlobal cache size: \(memGlobalCacheSize)\n"
            tempString += "\t\tGlobal cache line size: \(memCacheLineSize)\n"
        }
        
        tempString += "\tMaximum constant buffer size: \(maxConstBufferSize)\n"
        tempString += "\tMaximum constant arguments: \(maxConstArgs)\n"
        
        if (memType == UInt32(CL_LOCAL)) {
            tempString += "\tDevice has distinct local memory.\n"
            tempString += "\t\tLocal memory size: \(memSize)\n"
        }
        
        if( imageSupport == true ) {
            tempString += "\tImages supported\n"
            tempString += "\t\tMaximum samplers: \(maxImageSamplers)\n"
            tempString += "\t\tMaximum 2D dimensions: \(maxImage2dSize)\n"
            tempString += "\t\tMaximum 3D dimensions: \(maxImage3dSize)\n"
            tempString += "\t\tMaximum write image args: \(maxWriteImageArgs)\n"
            tempString += "\t\tMaximum read image args: \(maxReadImageArgs)\n"
        } else {
            tempString += "\tImages not supported\n"
        }
        
        tempString += "\tMaximum parameter size: \(maxParameterSize)\n"
        
        return tempString
    }
}