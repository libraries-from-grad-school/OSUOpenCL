//
//  OSUOpenCLKernel.swift
//  OSUOpenCL
//
//  Created by William Dillon on 2016-02-22.
//  Copyright © 2015-2016 Oregon State University (CEOAS). All rights reserved.
//  Read LICENSE in the top level directory for further licensing information.
//

import Foundation
import OpenCL
//import OSULogger

public class OSUOpenCLProgram {
    var clID: cl_program
    public let context: OSUOpenCLContext
    public var source: String?       = nil
    public var buildResults: String? = nil
    
    public enum BuildOption {
        case Define(String)
        case Include(String)
        case SinglePrecisionConstant
        case DenormsAreZero
        case DisableOptimization
        case EnableMAD
        case FiniteMathOnly
        case UnsafeMathOptimization
        case StrictAliasing
        case NoSignedZero
        case FastRelaxedMath
        case WarningsAreErrors
        case SupressWarnings
        
        func print() -> String {
            switch self {
            case .Define (let value):
                return "-D \(value) "
            case .Include (let value):
                return "-I \(value) "
            case .SinglePrecisionConstant:
                return "-cl-single-precision-constant "
            case .DenormsAreZero:
                return "-cl-denorms-are-zero "
            case .DisableOptimization:
                return "-opt-disable "
            case .EnableMAD:
                return "-mad-enable "
            case .FiniteMathOnly:
                return "-cl-finite-math-only "
            case .UnsafeMathOptimization:
                return "-cl-unsafe-math-optimizations "
            case .StrictAliasing:
                return "-cl-strict-aliasing "
            case .NoSignedZero:
                return "-no-signed-zeros "
            case .FastRelaxedMath:
                return "-cl-fast-relaxed-math "
            case .WarningsAreErrors:
                return "-Werror "
            case .SupressWarnings:
                return "-w "
            }
        }
    }

    private class func makeOptionsString(list: [BuildOption]) -> String {
        var output = ""
        for option in list {
            output += option.print()
        }
        
        return output
    }
    
    public init?(context: OSUOpenCLContext, source: String) {
        clID = nil
        self.context = context
        self.source  = source
        
        var error: cl_int = CL_SUCCESS
        let length: Array<size_t> = [size_t(source.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))]
        let charArray: ContiguousArray<UInt8> = source.nulTerminatedUTF8
        clID = charArray.withUnsafeBufferPointer { (inPtr: UnsafeBufferPointer<UInt8>) -> cl_program in
            var iPointer = UnsafePointer<Int8>(inPtr.baseAddress)
            return clCreateProgramWithSource(context.clID, 1, &iPointer, length, &error)
        }

        if clID == nil || error != CL_SUCCESS {
            //OSULogger.sharedLogger().log("Unable to create a new OpenCL kernel: \(OSUOpenCL.printCLError(error))", severity: .Error)
            return nil
        }
        
        return
    }
    
    public func compile(devices: [OSUOpenCLDevice], options: [OSUOpenCLProgram.BuildOption] = []) -> Bool {
        let optionsString = OSUOpenCLProgram.makeOptionsString(options).nulTerminatedUTF8
        var cl_devices = [cl_device_id](count: devices.count, repeatedValue: cl_device_id(bitPattern: 0))
        cl_devices = devices.map({ $0.clID })
        
        // Attempt to build the source
        let error: cl_int = optionsString.withUnsafeBufferPointer { (inPtr: UnsafeBufferPointer<UInt8>) -> cl_int in
            let iPointer = UnsafePointer<Int8>(inPtr.baseAddress)
            return clBuildProgram(self.clID, cl_uint(cl_devices.count), &cl_devices, iPointer, nil, nil)
        }

        if error != CL_SUCCESS {
            //OSULogger.sharedLogger().log("Unable to compile kernel source \(OSUOpenCL.printCLError(error))", severity: .Warning)
            return false
        } else {
            return true
        }
    }
    
    private func buildInfoString(devices devicesIn: [OSUOpenCLDevice] = [], param paramIn: Int32) -> String {
        // This will be the eventual output (baring any significant errors)
        var output = ""

        // We can get information for a single device, all devices, or some other subset
        // the devices parameter defaults to collecting the build logs from all devices
        var devices: [OSUOpenCLDevice] = []
        if devicesIn.count == 0 {
            devices = self.context.devices
        } else {
            devices = devicesIn
        }
        
        let param = cl_program_build_info(paramIn)
        for device in devices {
            var returnBufferSize: Int = 0
            
            // Try to get the length of the required buffer
            var retval: cl_int = clGetProgramBuildInfo(self.clID, device.clID, param, 0, nil, &returnBufferSize)
            if retval != cl_int(CL_SUCCESS) {
                //OSULogger.sharedLogger().log("Unable to query OpenCL: \(OSUOpenCL.printCLError(retval))", severity: .Warning)
                continue
            }
            
            // Allocate a buffer to contain the value (which is UTF8 string encoded)
            var buffer: [CChar] = Array<CChar>(count: Int(returnBufferSize),
                repeatedValue: CChar(0x00))
            
            // Attempt to retreive the buffer
            retval = clGetProgramBuildInfo(self.clID, device.clID, param, returnBufferSize, &buffer, nil)
            if retval != cl_int(CL_SUCCESS) {
                //OSULogger.sharedLogger().log("Unable to query OpenCL: \(OSUOpenCL.printCLError(retval))", severity: .Warning)
                continue
            }
            
            if let str = String(UTF8String: buffer) {
                output += str
            }
        }
        
        return output
    }

    public func buildLog(devices devicesIn: [OSUOpenCLDevice] = []) -> String {
        return buildInfoString(devices: devicesIn, param: CL_PROGRAM_BUILD_LOG)
    }
    
    public func kernels() -> [OSUOpenCLKernel]? {
        var retval: cl_int = CL_SUCCESS

        // Get the number of kernels available in the program
        var numKernels: cl_uint = 0
        retval = clCreateKernelsInProgram(clID, 0, nil, &numKernels)
        if retval != CL_SUCCESS {
            //OSULogger.sharedLogger().log("Unable create kernels from program: \(OSUOpenCL.printCLError(retval))", severity: .Warning)
            return nil
        }
        
        // Create storage for the kernel IDs and get them
        var kernelIDs: [cl_kernel] = Array<cl_kernel>(count: Int(numKernels), repeatedValue: nil)
        retval = clCreateKernelsInProgram(clID, numKernels, &kernelIDs, nil)
        if retval != CL_SUCCESS {
            //OSULogger.sharedLogger().log("Unable create kernels from program: \(OSUOpenCL.printCLError(retval))", severity: .Warning)
            return nil
        }
        
        // Create Swift objects for each of the kernel IDs.
        var kernels = [OSUOpenCLKernel]()
        for cl_kernel in kernelIDs {
            do {
                let kernel = try OSUOpenCLKernel(context: self.context, program: self, clID: cl_kernel)
                kernels.append(kernel)
            } catch {
                //OSULogger.sharedLogger().log("Unable to create kernel object from kernel ID.", severity: .Warning)
            }
        }
        
        return kernels
    }
}

public enum AddressQualifier {
    case cl_global
    case cl_local
    case cl_constant
    case cl_private
    case unknown
    
    func fromRaw(input:Int32?) -> AddressQualifier {
        switch(input) {
        case .Some(CL_KERNEL_ARG_ADDRESS_GLOBAL):   return .cl_global
        case .Some(CL_KERNEL_ARG_ADDRESS_LOCAL):    return .cl_local
        case .Some(CL_KERNEL_ARG_ADDRESS_CONSTANT): return .cl_constant
        case .Some(CL_KERNEL_ARG_ADDRESS_PRIVATE):  return .cl_private
        case nil: return .unknown
        default:  return .unknown
        }
    }
}

public enum AccessQualifier {
    case readOnly
    case writeOnly
    case readWrite
    case none
    case unknown
    
    func fromRaw(input:Int32?) -> AccessQualifier {
        switch(input) {
        case .Some(CL_KERNEL_ARG_ACCESS_READ_ONLY):  return .readOnly
        case .Some(CL_KERNEL_ARG_ACCESS_WRITE_ONLY): return .writeOnly
        case .Some(CL_KERNEL_ARG_ACCESS_READ_WRITE): return .readWrite
        case .Some(CL_KERNEL_ARG_ACCESS_NONE):       return .none
        case nil: return .unknown
        default:  return .unknown
        }
    }
}

public enum TypeQualifier {
    case none
    case pipe // Not implemented in this platform
    case standard(const: Bool, restrict: Bool, volatile: Bool)
    case unknown
    
    func fromRaw(input:cl_bitfield?) -> TypeQualifier {
        if let unwrapped = input {
            let coerced = Int32(unwrapped)
            
            // The value can either be "none" or a combination the below
            if Int32(coerced | CL_KERNEL_ARG_TYPE_NONE) > 0 {
                return none
            }
            
        }
        return .unknown
    }
}

public struct OSUOpenCLArgument {
    public let kernel: OSUOpenCLKernel
    let index:  UInt
    public let name:   String
    public let type:   String
    public var addressQualifier: AddressQualifier  = .unknown
    public var accessQualifier: AccessQualifier    = .unknown
    public var typeQualifier: TypeQualifier        = .unknown
    
    private static func getArgInfoString(kernelID: cl_kernel, argID: UInt, param: cl_kernel_arg_info) throws -> String {
        var retval: cl_int = CL_SUCCESS
        var stringLength: Int = 0
        
        retval = clGetKernelArgInfo(kernelID, cl_uint(argID), param, 0, nil, &stringLength)
        if retval != CL_SUCCESS {
            throw OSUOpenCLError.kernelInitializeError(optional: "Unable to retreieve argument parameter: \(OSUOpenCL.printCLError(retval))")
        }
        
        var chars = [CChar](count: Int(stringLength), repeatedValue: CChar(0))
        retval = clGetKernelArgInfo(kernelID, cl_uint(argID), param, stringLength, &chars, nil)
        if retval != CL_SUCCESS {
            throw OSUOpenCLError.kernelInitializeError(optional: "Unable to retreieve argument parameter: \(OSUOpenCL.printCLError(retval))")
        }
        
        if let str = String(UTF8String: &chars) {
            return str
        } else {
            throw OSUOpenCLError.kernelInitializeError(optional: "Unable to convert byte buffer to string.")
        }
    }
    
    private static func getArgInfoScalar<T: IntegerLiteralConvertible>(kernelID: cl_kernel, argID: UInt, param: cl_kernel_arg_info) throws -> T {
        var returnBufferSize: size_t = 0
        var retval = clGetKernelArgInfo(kernelID, cl_uint(argID), param, 0, nil, &returnBufferSize)
        if retval != CL_SUCCESS {
            throw OSUOpenCLError.kernelInitializeError(optional: "Unable to retreieve argument parameter: \(OSUOpenCL.printCLError(retval))")
        }
        
        assert(returnBufferSize == size_t(sizeof(T)),
            "Type size mismatch while querying OpenCL. \(returnBufferSize) != \(sizeof(T)).")
        
        var output: T = 0
        retval = clGetKernelArgInfo(kernelID, cl_uint(argID), param, returnBufferSize, &output, nil)
        
        if retval != CL_SUCCESS {
            throw OSUOpenCLError.kernelInitializeError(optional: "Unable to retreieve argument parameter: \(OSUOpenCL.printCLError(retval))")
        }
        
        return output
    }

    public init(kernel: OSUOpenCLKernel, index: UInt) throws {
        self.kernel = kernel
        self.index = index

        do {
            name  = try OSUOpenCLArgument.getArgInfoString(kernel.clID, argID: index, param: cl_kernel_arg_info(CL_KERNEL_ARG_NAME))
            type  = try OSUOpenCLArgument.getArgInfoString(kernel.clID, argID: index, param: cl_kernel_arg_info(CL_KERNEL_ARG_TYPE_NAME))
            addressQualifier = try addressQualifier.fromRaw(OSUOpenCLArgument.getArgInfoScalar(kernel.clID, argID: index, param: cl_kernel_arg_info(CL_KERNEL_ARG_ADDRESS_QUALIFIER)))
            accessQualifier  = try accessQualifier.fromRaw(OSUOpenCLArgument.getArgInfoScalar(kernel.clID, argID: index, param: cl_kernel_arg_info(CL_KERNEL_ARG_ACCESS_QUALIFIER)))
            typeQualifier    = try typeQualifier.fromRaw(OSUOpenCLArgument.getArgInfoScalar(kernel.clID, argID: index, param: cl_kernel_arg_info(CL_KERNEL_ARG_TYPE_QUALIFIER)))
        } catch let error {
            throw error
        }
    }
}

public struct OSUOpenCLKernel {
    let clID: cl_kernel
    public let context: OSUOpenCLContext
    public let program: OSUOpenCLProgram
    public let name: String
    public var arguments = [OSUOpenCLArgument]()
    
    private static func getKernelName(clID: cl_kernel) throws -> String {
        var retval: cl_int = CL_SUCCESS
        var nameLength: Int = 0
        retval = clGetKernelInfo(clID, cl_kernel_info(CL_KERNEL_FUNCTION_NAME), 0 , nil, &nameLength)
        if retval != CL_SUCCESS {
            throw OSUOpenCLError.kernelInitializeError(optional: "Unable to retreieve kernel name: \(OSUOpenCL.printCLError(retval))")
        }
        
        var nameChars: [CChar] = [CChar](count: Int(nameLength), repeatedValue: CChar(0))
        retval = clGetKernelInfo(clID, cl_kernel_info(CL_KERNEL_FUNCTION_NAME), nameLength, &nameChars, nil)
        if retval != CL_SUCCESS {
            throw OSUOpenCLError.kernelInitializeError(optional: "Unable to retreieve kernel name: \(OSUOpenCL.printCLError(retval))")
        }
        
        if let str = String(UTF8String: &nameChars) {
            return str
        } else {
            throw OSUOpenCLError.kernelInitializeError(optional: "Unable to convert byte buffer to string.")
        }
    }
    
    public init(context: OSUOpenCLContext, program: OSUOpenCLProgram, clID: cl_kernel) throws {
        self.context = context
        self.program = program
        self.clID = clID

        do {
            // Get the name for this kernel
            self.name = try OSUOpenCLKernel.getKernelName(clID)
            
            // We need to get the number of arguments, then query for them individually
            var numArgs: cl_uint = 0
            var retval: cl_int = CL_SUCCESS
            retval = clGetKernelInfo(clID, cl_kernel_info(CL_KERNEL_NUM_ARGS), Int(sizeof(cl_uint)), &numArgs, nil)
            if retval != CL_SUCCESS {
                throw OSUOpenCLError.kernelInitializeError(optional: "Unable to get kernel info: \(OSUOpenCL.printCLError(retval))")
            }
            
            do {
                for i in 0 ..< UInt(numArgs) {
                    arguments.append(try OSUOpenCLArgument(kernel: self, index: i))
                }
            } catch let error {
                throw error
            }
        }
    }
}